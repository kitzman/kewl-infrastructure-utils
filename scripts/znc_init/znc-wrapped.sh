#!/usr/bin/env bash

# Copyright (C) 2021 kewl-infrastructure-utils Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

: ${command_user:=${command_user:-"znc"}}
znc_command=/usr/bin/znc
znc_command_args=$@

: ${datadir:=${datadir:-"/var/lib/znc"}}

znc_uid=$(id -u $command_user)
znc_gid=$(id -g $command_user)

(exec bwrap \
     --ro-bind /bin /bin \
     --ro-bind /usr/bin/znc /usr/bin/znc \
     --ro-bind /lib /lib \
     --ro-bind /usr/lib /usr/lib \
     --ro-bind /etc/ssl /etc/ssl \
     --ro-bind /usr/share/ca-certificates /usr/share/ca-certificates \
     --ro-bind /usr/share/znc /usr/share/znc \
     --bind $datadir $datadir \
     --dir /home/znc \
     --symlink $datadir /home/znc/.znc \
     --dir /tmp \
     --symlink ../tmp var/tmp \
     --proc /proc \
     --dev /dev \
     --ro-bind /etc/resolv.conf /etc/resolv.conf \
     --chdir / \
     --unsetenv HOME \
     --unshare-all \
     --share-net \
     --die-with-parent \
     --dir /run/user/$znc_uid \
     --file 11 /etc/passwd \
     --file 12 /etc/group \
     $znc_command $znc_command_args) \
     11< <(getent passwd $znc_uid) \
     12< <(getent group $znc_gid)
