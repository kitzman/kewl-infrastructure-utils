#!/bin/sh

# Copyright (C) 2021 kewl-linode-tf-modz Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

set -e

cd $HOME

apk add podman podman-openrc podman-bash-completion

subuid_file=/etc/subuid
subgid_file=/etc/subgid

# should most probably be root tho
me=$(whoami)
me_uid=$(id -u)
me_gid=$(id -g)

min_id=65536
base_count=64

sed "s/$me:0:0.*\$//g" -i $subuid_file
sed "s/$me:0:0.*\$//g" -i $subgid_file

# should be configured by the user
if [ -z $(cat $subuid_file | awk -F: "{if (\$1 == \"$me\") {print;} }") ]; then
    usermod -v \
	    $(($me_uid * base_count + min_id))-$(($me_uid * base_count + min_id + base_count - 1)) \
	    $me
fi

if [ -z $(cat $subgid_file | awk -F: "{if (\$1 == \"$me\") {print;} }") ]; then
   usermod -w \
	    $(($me_gid * base_count + min_id))-$(($me_gid * base_count + min_id + base_count - 1)) \
	    $me
fi

echo 'overlay' > /etc/modules-load.d/podman.conf

if [ -z $(cat /etc/fstab | grep cgroup) ]; then
    echo "cgroup2 /sys/fs/cgroup cgroup2 defaults 0 0" >> /etc/fstab
fi
