#!/bin/sh

# Copyright (C) 2021 kewl-infrastructure-utils Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

cd $HOME

sed '$ahttp://dl-cdn.alpinelinux.org/alpine/v3.14/testing' -i /etc/apk/repositories

apk update
apk upgrade

echo "" > /etc/motd

groupadd abuild
useradd -G -m wheel,users abuild

apk add ca-certificates ca-certificates-bundle
apk add syslog-ng logrotate bash

rc-update del syslog boot
rc-update add syslog-ng boot
rc-update add cron

rc-service syslog stop
rc-service syslog-ng start
rc-service crond start
