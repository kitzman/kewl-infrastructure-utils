#!/bin/sh

# Copyright (C) 2021 kewl-infrastructure-utils Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

set -e

apk add znc znc-extra znc-backlog

rc-update del znc
rc-update add znc-wrapped

rc-service znc-wrapped start
