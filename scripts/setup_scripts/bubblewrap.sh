#!/bin/sh

# Copyright (C) 2021 kewl-infrastructure-utils Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

set -e

cd $HOME

rm -rf $HOME/staging/bubblewrap || true
rm -rf $HOME/packages/staging/x86_64/APKINDEX* || true
rm -rf $HOME/packages/staging/x86_64/*bubblewrap* || true

mkdir -p $HOME/staging/bubblewrap
cd $HOME/staging/bubblewrap

curl -LO https://gitlab.alpinelinux.org/alpine/aports/-/raw/master/main/bubblewrap/APKBUILD

sed 's/options="\(.*\)"/options="\1 suid"/g' -i APKBUILD
sed 's/--with-priv-mode=\([a-z]\)* \\/--with-priv-mode=setuid/g' -i APKBUILD
sed 's/.*enable-require-userns.*//g' -i APKBUILD

abuild-keygen -a -n

abuild -F -r

cd $HOME/packages/staging/x86_64

apk add --allow-untrusted bubblewrap-*.apk

cd $HOME
