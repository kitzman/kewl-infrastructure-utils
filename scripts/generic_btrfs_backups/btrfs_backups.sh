#!/bin/sh

set -e

backup_filename=".snapshot-$(date -I)"

#
# runtime
#

progname=$(basename $0)

if [[ "$progname" != backup\.* ]]; then
    echo "malformed name"
    exit 1
fi

jobname=${progname:7}

#
# configuration
#

. /etc/conf.d/backup.$jobname

logger_ident="${logger_ident:-backups}"
backup_retency="${backup_retency:-10}"
backup_qgroupid="${backup_qgroupid:-}"

if [ -z "$target_backups_path" ]; then
    echo "target_backups_path not set!"
fi

if [ -z "$source_storage_path" ]; then
    echo "source_storage_path not set!"
fi

#
# utils & sanity checks
#

function log() { echo $@; logger -t $logger_ident $@; }

if [ ! -d "$target_backups_path" ]; then
	log "$target_backups_path does not exist"
	exit 1
fi

if [ ! -d "$source_storage_path" ]; then
	log "$source_storage_path does not exist"
	exit 1
fi

parent_backup=$(find $source_storage_path -maxdepth 1 -type d -name ".snapshot*")

if [ ! -z "$parent_backup" ]; then
    parent_backup=$(basename $parent_backup)
fi

#
# Removing old backups
#

log "removing old backups from $target_backups_path"

for backup_vol_path in $(find $target_backups_path -maxdepth 1 -type d \
			 -name ".snapshot*" | \
			sort | head -n "-$backup_retency"); do

    backup_vol=$(basename $backup_vol_path)

    log "removing $backup_vol"
    backup_volid=$(btrfs sub list $target_backups_path | \
		       grep $backup_vol | awk '{print $2}')
    btrfs sub del $target_backups_path/$backup_vol

    qgroup_id="0/$backup_volid"

    btrfs qgroup destroy $qgroup_id || log "could not find qgroup $qgroup_id"
done

#
# Create a new local snapshot
#

log "creating an interim backup in $source_storage_path"
btrfs subvolume snapshot -r \
	$source_storage_path \
	$source_storage_path/$backup_filename

#
# Send it
#

log "sending backups from $source_storage_path to $target_backups_path"

if [ ! -z "$parent_backup" ]; then
    log "...with parent"
    btrfs send \
	  -p $source_storage_path/$parent_backup \
	  $source_storage_path/$backup_filename | \
	btrfs receive $target_backups_path/
else
    log "... without parent"
    
    btrfs send $source_storage_path/$backup_filename | \
	btrfs receive $target_backups_path/
fi

if [ ! -z "$backup_qgroupid" ]; then
    log "assigning qgroup $backup_qgroupid"
    backup_volid=$(btrfs sub list $target_backups_path | \
		       grep $backup_filename | awk '{print $2}')

    btrfs qgroup assign --rescan "0/$backup_volid" "$backup_qgroupid" $target_backups_path
fi

#
# Remove parent backup
#

log "removing old backups from $source_storage_path"

if [ ! -z "$parent_backup" ]; then
    log "removing $source_storage_path/$parent_backup"

    backup_volid=$(btrfs sub list $target_backups_path | \
		       grep $backup_vol | awk '{print $2}')
    btrfs subvolume delete $source_storage_path/$parent_backup

    qgroup_id="0/$backup_volid"

    btrfs qgroup destroy $qgroup_id || log "could not find qgroup $qgroup_id"
fi

#
# Final sync
#

log "sync-ing and exiting"

btrfs subvolume sync $source_storage_path
btrfs subvolume sync $target_backups_path
