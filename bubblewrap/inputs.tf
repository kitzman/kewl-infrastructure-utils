# Copyright (C) 2021 kewl-infrastructure-utils Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

# Server-related variables
variable "server_host" {
  type        = string
  description = "The host to which ZNC should be installed and configured"
}
