# Copyright (C) 2021 kewl-infrastructure-utils Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

resource "null_resource" "provisioned_server" {
  connection {
    host  = var.server_host
    port  = 22
    user  = "root"
    agent = true
  }

  provisioner "remote-exec" {
    script = "../scripts/setup_scripts/bubblewrap.sh"
  }
}
