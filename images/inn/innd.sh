#!/bin/sh

if [ ! -z "$INND_FIRST_RUN" ]; then
    makehistory
fi

/usr/bin/rc.news

sleep 3s

innd_pid=$(cat /var/run/inn/innd.pid)

/bin/sh -c '/usr/bin/nnrpd -D -p 563 -S'

tail -f --pid=$innd_pid /var/log/inn/news &
tail -f --pid=$innd_pid /var/log/inn/errlog
