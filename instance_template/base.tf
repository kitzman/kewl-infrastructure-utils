# Copyright (C) 2021 kewl-infrastructure-utils Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

terraform {
  required_providers {
    linode = {
      source  = "linode/linode"
      version = "1.24.0"
    }
  }
}

provider "linode" {
  token = var.linode_token
}

resource "linode_sshkey" "default_key" {
  label   = var.sshkey_label
  ssh_key = chomp(file(var.sshkey_pub_path))
}
