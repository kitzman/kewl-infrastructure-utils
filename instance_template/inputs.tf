# Copyright (C) 2021 kewl-infrastructure-utils Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

# Linode-specific inputs
variable "linode_token" {
  type        = string
  description = "Linode token"
  sensitive   = true
}

# Instance-specific variables
variable "instance_name" {
  type        = string
  description = "Name of the instance"
}

variable "instance_type" {
  type        = string
  description = <<EOF
Type of Linode instance to deploy.

Choices: g6-nanode-1, g6-standard-1, g6-standard-2, g6-standard-4,
g6-standard-6, g6-standard-8, g6-standard-16, g6-standard-20,
g6-standard-24, g6-standard-32, g7-highmem-1, g7-highmem-2,
g7-highmem-4, g7-highmem-8, g7-highmem-16, g6-dedicated-2,
g6-dedicated-4, g6-dedicated-8, g6-dedicated-16, g6-dedicated-32,
g6-dedicated-48, g6-dedicated-50, g6-dedicated-56, g6-dedicated-64,
g1-gpu-rtx6000-1, g1-gpu-rtx6000-2, g1-gpu-rtx6000-3, g1-gpu-rtx6000-4
EOF
}

variable "region" {
  type        = string
  description = <<EOF
Linode region/availability zone.

Choices: ap-west, ca-central, ap-southeast, us-central, 
us-west, us-southeast, us-east, eu-west, ap-south, 
eu-central, ap-northeast
EOF
}

# Locally-scoped variables
variable "sshkey_label" {
  type        = string
  description = "Label to use for the SSH key"
  default     = "default_terraform_key"
}

variable "sshkey_pub_path" {
  type        = string
  description = "Path to the SSH public key"
}

variable "default_root_password" {
  type        = string
  description = "Default root password"
  sensitive   = true
}

# ZNC configuration
variable "irc_port" {
  type        = string
  description = "The IRC port to allow"
  default     = "6697"
}
