# Copyright (C) 2021 kewl-infrastructure-utils Authors kitzman
# SPDX-License-Identifier: GPL-3.0+

resource "linode_instance" "main_instance" {
  label  = var.instance_name
  image  = "linode/alpine3.14"
  region = var.region
  type   = var.instance_type

  tags = ["internal"]

  authorized_keys = [linode_sshkey.default_key.ssh_key]
  root_pass       = var.default_root_password

  private_ip      = true
  backups_enabled = false

  provisioner "remote-exec" {
    script = "../scripts/setup_scripts/alpine.sh"
  }
}

resource "linode_firewall" "main_firewall" {
  label = "${var.instance_name}-fw"

  inbound {
    label = "accept-inbound-SSH"

    protocol = "TCP"
    ports    = "22"
    ipv4     = ["0.0.0.0/0"]
    ipv6     = ["::/0"]
    action   = "ACCEPT"
  }

  inbound {
    label = "accept-inbound-IRC"

    protocol = "TCP"
    ports    = var.irc_port
    ipv4     = ["0.0.0.0/0"]
    ipv6     = ["::/0"]
    action   = "ACCEPT"
  }

  inbound_policy  = "DROP"
  outbound_policy = "ACCEPT"

  linodes = [linode_instance.main_instance.id]
}

module "bubblewrap" {
  source = "../bubblewrap"

  server_host = linode_instance.main_instance.ip_address
}

module "podman" {
  source = "../znc"

  server_host = linode_instance.main_instance.ip_address
}
