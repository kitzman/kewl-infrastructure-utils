# Mods for your everyday needs

This repo contains modules to provision Alpine 3.14 VMs with some
services, in case you need something more lightweight, easy to configure
with `syslog-ng`.

# Modules

There are currently modules for:

	- podman
	- scripts for running a container as an rc service
	- building a setuid bubblewrap without userns'es by default

Additionally, you can find some bubblewrapped znc init scripts, in case you'd like
to run ZNC without Podman.

# OpenRC containers

The first step would be copying the container init script from the `scripts` directory.

The code can easily serve as documentation: one only has to link
`/etc/init.d/container.mycontainer` to `/etc/init.d/container`.

Then, to configure the way the container is ran, modify
`/etc/conf.d/container.mycontainer`.

# Podman images

## ZNC

Build the image, tag it, and use the OpenRC scripts provided, as an example.
A sample configuration can be found in `./scripts/znc/container.znc.conf`.

## INN (NNTP newsserver)

Another image which comes with an example configuration. One would have to take
care of configuring INN beforehand, though.

The APKBUILD will be sent to the aports repo... one day™.

## Lieferhund + sinntp scripts

My news processor. Currently can only read RSS and Atom. I scripted it to send
the news to an NNTP server (one line command with sinntp - and the news item
has the same structure as an email, so you can just use the interpolator).

**NOTE**: It has to compile GnuTLS - because the Alpine package does not provide
Guile bindings, which are a prerequisite for running Lieferhund.
